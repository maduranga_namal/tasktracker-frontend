import http from "../http-common";

class TaskDataService{

    getAllTasks(email) {
        return http.get(`/tasks/${email}`);
    }

    addenwTask(data) { 
        return http.post("/task", data);
    }

    deleteTask(id) { 
        return http.delete(`/task/${id}`);
    }

    getTask(id) {
        return http.get(`/task/${id}`)
    }
}

export default new TaskDataService();