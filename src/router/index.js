import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '/',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/sign-in',
    component: () => import('../views/SignIn.vue')
  },
  {
    path: '/tasktracker',
    component: () => import('../views/taskTracker.vue')
  } 
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
