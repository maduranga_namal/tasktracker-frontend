import { createApp, Vue } from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase' 
import axios from 'axios'
import VueAxios from 'vue-axios'



var firebaseConfig = {
    apiKey: "AIzaSyCcRKmslm76w5qnxz3ka2SXJQ4YL0yrDF4",
    authDomain: "task-tracker-application.firebaseapp.com",
    projectId: "task-tracker-application",
    storageBucket: "task-tracker-application.appspot.com",
    messagingSenderId: "1011605495891",
    appId: "1:1011605495891:web:b34a660424ee0c9d3b26a6"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


createApp(App).use(router, VueAxios, axios).mount('#app')
